# Bank_Passwords

**Passo a passo para utilização do sistema**

- O sistema possui dois principais componentes a seção do usuário e o monitor de senhas, especificados na imagem abaixo.
    -   A **seção do usuário** possui as funções de cada usuário e suas respectivas ações.
    -   O **monitor de senhas** possui a função de mostrar a última senha que foi chamada e todas as senhas em espera.


![Alt text](/screenshots/screenshot_01.png?raw=true "Componentes")


- No componente Seção do usuário, o usuário pode alternar entre os tipos de usuários(Cliente e Gerente) através do ícone sinalizado na imagem abaixo. 
- Cada tipo de usuário possui suas respectivas ações.
    - **Clientes** podem fazer a retirada de dois tipos de senha: "prioritarias" e "Normais".
    - **Gerentes** podem fazer a chamada das senhas e reiniciar a contagem das mesmas.

![Alt text](/screenshots/screenshot_02.png?raw=true "users")

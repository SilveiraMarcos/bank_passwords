package com.paripassu.ws.rest.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ServiceConnection {
	public static Connection getConnection() {
		Connection con = null;
		
		try {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
					System.out.println("Erro - " + e.getMessage());
			}
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/paripassubank_db", "postgres","admin");
		}catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
		
		return con;
	}
}

package com.paripassu.ws.rest.beans;

public class Password {
	
	private int id;
	private String password;
	private String type;
	
	public int getId() {
		return id;
	}
	public void setId(int string) {
		this.id = string;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}

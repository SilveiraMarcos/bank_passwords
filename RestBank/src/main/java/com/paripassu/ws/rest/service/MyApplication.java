package com.paripassu.ws.rest.service;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;

public class MyApplication extends ResourceConfig {


    public MyApplication() {

        packages("com.paripassu.ws.rest.service");
        register(LoggingFilter.class);
        register(CORSFilter.class);
        register(ServiceBank.class);
    }
}
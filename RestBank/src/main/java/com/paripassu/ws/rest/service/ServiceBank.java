package com.paripassu.ws.rest.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.paripassu.ws.rest.beans.LastPassword;
import com.paripassu.ws.rest.beans.Password;

@Path("/ParipassuBank")
public class ServiceBank{
	

    @Context
    HttpServletRequest request;
		
	private Connection con = ServiceConnection.getConnection();
	
	@POST
	@Path("/SetPassword")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response setPassword(Password password) {
		String sql = "insert into tbpasswords(password, type) values(?,?);";
		ArrayList<String> response = new ArrayList<String>();
		try {
			PreparedStatement preparing = con.prepareStatement(sql);
			preparing.setString(1, password.getPassword());
			preparing.setString(2, password.getType());
			preparing.execute();
			preparing.close();
			response.add("Inserted!");
		} catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
		
		return Response.status(201)
				.entity(response)
				.build();
	}
	
	@GET
	@Path("/GetAllPasswords")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllPasswords() {			
		ArrayList<Password> objPriority = new ArrayList<Password>();
		ArrayList<Password> objNormally = new ArrayList<Password>();
		ArrayList<Password> objResponse = new ArrayList<Password>();
		
  	  	Password objPasswordP;
  	  	Password objPasswordN;
				
		String sql = "select * from tbpasswords";
		try {
			PreparedStatement preparing = con.prepareStatement(sql);
			ResultSet rs =  preparing.executeQuery();
			
			
			while (rs.next ()) {			      
			      if(rs.getString(3).equals("P")) {			    	  
			    	  objPasswordP = new Password();
			    	  objPasswordP.setId(rs.getInt(1));
			    	  objPasswordP.setPassword(rs.getString(2));
			    	  objPasswordP.setType(rs.getString(3));
			    	  
			    	  objPriority.add(objPasswordP);
			      }else {
			    	  objPasswordN = new Password();
			    	  objPasswordN.setId(rs.getInt(1));
			    	  objPasswordN.setPassword(rs.getString(2));
			    	  objPasswordN.setType(rs.getString(3));
			    	  
			    	  objNormally.add( objPasswordN);
			      }
			}
		} catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
				
		objResponse.addAll(objPriority);
		objResponse.addAll(objNormally);
		
		return Response.status(200).entity(objResponse).build();
	}
	
	@DELETE
	@Path("/Reset")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response reset() {
		String sql = "delete from tbpasswords";
		try {
			PreparedStatement preparing = con.prepareStatement(sql);
			preparing.execute();
			preparing.close();
		} catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
		
		return Response.status(201).build();
	}
	
	//deleta a ultima senha chamada e a insere na tabela de ultimas senhas
	@DELETE
	@Path("/Next/{id}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response next(final @PathParam("id") int id) {
		ArrayList<String> response = new ArrayList<String>();
		
		String deleteSql = "delete from tbpasswords WHERE id = ?";
		String selectSql = "select * from tbpasswords where id = ?";
		String sqlInsert = "insert into tblastpass(last) values(?);";
		
		String lastSelected = "";
		
		try {
			PreparedStatement preparingSelect = con.prepareStatement(selectSql);
			preparingSelect.setInt(1, id);
			ResultSet rs =  preparingSelect.executeQuery();
			while (rs.next ()) {			      
				lastSelected = rs.getString(2);
			}
			
			PreparedStatement preparingDelete = con.prepareStatement(deleteSql);
			preparingDelete.setInt(1, id);
			preparingDelete.execute();
			preparingDelete.close();
			
			PreparedStatement preparingInsert = con.prepareStatement(sqlInsert);
			preparingInsert.setString(1, lastSelected);
			preparingInsert.execute();
			preparingInsert.close();
			
			response.add("Inserted!");
		} catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
		
		return Response.status(201).entity(response).build();
	}
	
	@GET
	@Path("/getAllLastsPasswords")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllLastsPasswords() {
		ArrayList<LastPassword> objResponse = new ArrayList<LastPassword>();
		LastPassword objLastPass;
		String selectLastSql = "select * from tblastpass;";
	
		try {
			PreparedStatement preparingSelectLast = con.prepareStatement(selectLastSql);
			ResultSet rls =  preparingSelectLast.executeQuery();
			while (rls.next ()) {	
				
				objLastPass = new LastPassword();
				
				objLastPass.setId(rls.getInt(1));
				objLastPass.setLast(rls.getString(2));
				objResponse.add(objLastPass);
			}
			
		} catch (SQLException e) {
			System.out.println("Erro - " + e.getMessage());
		}
		
		return Response.status(200).entity(objResponse).build();	
	}	
	
}

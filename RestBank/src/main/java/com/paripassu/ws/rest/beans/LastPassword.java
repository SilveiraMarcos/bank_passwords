package com.paripassu.ws.rest.beans;
public class LastPassword {
	
	private int id;
	private String last;
	
	public int getId() {
		return id;
	}
	
	public void setId(int string) {
		this.id = string;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}
}

export const REFRESH_PASSWORDS = "REFRESH_PASSWORDS";
export const LAST_PASSWORD = "LAST_PASSWORD";

export const refreshPasswords = (List) => { 
    return {
        type: REFRESH_PASSWORDS,
        payload: List
    }
}

export const lastPassword = (password) => ({
    type: LAST_PASSWORD,
    payload: password
})
import { passwords } from './passwords';
import { combineReducers } from 'redux';

export const Reducers = combineReducers({
    passwords: passwords
});

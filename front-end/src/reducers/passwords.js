import {
    LAST_PASSWORD, 
    REFRESH_PASSWORDS
} from "../actions/passwordsActions";

const initialState = {
    listPasswords: ["Loading!"]
};

export const passwords = (state = initialState, action) =>{
    switch(action.type){
        case REFRESH_PASSWORDS: 
            return {
                ...state,
                listPasswords: action.payload
            };
        case LAST_PASSWORD:

            return {
                ...state,
                password: action.payload
            }
        default:
            return state;
    }
}
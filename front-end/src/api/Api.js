import axios from "axios";

const BASE_URL = 'http://localhost:8080/RestBank/rest/ParipassuBank/';
const withBaseUrl = path => `${BASE_URL}${path}`

export class Api{
    static getAllPasswords() {
        return axios.get(withBaseUrl('GetAllPasswords'))
    }

    static setPassword(password, type){
        return axios.post(withBaseUrl('SetPassword'), {"password": password, "type": type});
    }

    static Reset(){
        return axios.delete(withBaseUrl('Reset'));
    }

    static Next(id){
        if(!id)return;
        
        return axios.delete(withBaseUrl('Next')+`/${id}`);
    }

    static getAllLastsPasswords() {
        return axios.get(withBaseUrl('getAllLastsPasswords'))
    }
    
}
import { Monitor } from "../components/Monitor/Monitor"
import { User } from "../components/User/User"

export const Home = ()=>{
    return (
        <>  
            <User/>
            <Monitor/>
        </>
    )
}
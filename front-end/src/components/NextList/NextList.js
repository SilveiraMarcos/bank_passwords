import { useSelector } from "react-redux";
import "./NextList.css"

export const NextList = ()=>{
    const List = useSelector((state)=> state.passwords.listPasswords);
    return (
        <div className="containerList">  
            <h3>TODAS AS SENHAS</h3>
            <ul>
                {List.map( item =>
                    <li key={"ID"+item.id} className={item.type === "P"? "priority": "normaly"}>{item.password}</li>
                )}  
            </ul>
        </div>
    )
}
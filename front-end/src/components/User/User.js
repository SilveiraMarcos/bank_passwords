import "./User.css"
import {BsArrowRepeat, BsFillCaretRightFill, BsFillXCircleFill} from "react-icons/bs";
import { useEffect, useState } from "react";
import { Api } from "../../api/Api";

import { useDispatch } from "react-redux";
import { refreshPasswords, lastPassword } from "../../actions/passwordsActions"

export const User = ()=>{
    const [userType, setUserType] = useState(false);
    const [passwords, setPasswords] = useState([]);
    const dispatch = useDispatch();
    const fetchPasswords = async () => {
        const {data} = await Api.getAllPasswords();
        dispatch(refreshPasswords(data))
        setPasswords(data)
    }

    const fetchLastPasswords = async () =>{
        const {data} = await Api.getAllLastsPasswords();
        dispatch(lastPassword(data));
    }

    const generatePassword = (type)=>{
        var randomDigits = getRandomInt(0, 999);
        const qtdDigit = (''+randomDigits+1).length;
        var password = type;
        for(var i=4; i > qtdDigit; i--){

            password +="0";
        };

        password += randomDigits;

        if(passwords.length !== 0){
            passwords.forEach(item => {
                if(item === password){
                    generatePassword(type);
                }
            });
        }

        insertPasswords(password,type);
    }

    const insertPasswords = async (password, type) => {
        await Api.setPassword(password, type);
        fetchPasswords();
    }

    const Reset = async () => {
        await Api.Reset();
        fetchPasswords();
    }

    const Next = async (id)=>{
        if(!id) return;
        await Api.Next(id);
        fetchPasswords();
    }

    const getRandomInt = (min, max) =>{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    
    useEffect(()=>{
        fetchPasswords();
        fetchLastPasswords();
    });

    return (
        <div className="containerUser">
            <img 
                src={userType ? "https://cdn.icon-icons.com/icons2/37/PNG/512/manager_3977.png" : "https://monomousumi.com/wp-content/uploads/anonymous-user-5.png"} 
                alt="Lamp" width="200" height="200" className="user"/>

            <div className="contPerfil">
                <h3 className="namePerfil">{userType? "Gerente":"Cliente"} </h3>
                
                <div className="tooltip" onClick={() => setUserType(!userType)} >
                    <BsArrowRepeat className="iconBsArrowRepeat" id="Tooltip"/>
                    <span className="tooltiptext">Alterar Usuário!</span>
                </div>

            </div>
            <div className="contButton">
                <button 
                    className="button b1"
                    onClick={() => userType ? Next(passwords[0]?.id) : generatePassword("P")}>
                    {userType?"Chamar " :"Prioritaria"}
                    {userType?<BsFillCaretRightFill/>:""}
                </button>

                <button 
                    className={userType? "button b2" : "button b3"}
                    onClick={() => userType ?  Reset() : generatePassword("N")}>
                    {userType? "Reiniciar":"Normal"} 
                    {userType?<div className="contIcon"><BsFillXCircleFill/></div>:""}
                    
                </button>
            </div>
        </div>
    )
}
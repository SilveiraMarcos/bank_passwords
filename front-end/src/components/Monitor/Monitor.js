import { LastPassword } from '../LastPassword/LastPassword';
import { NextList } from '../NextList/NextList';
import './Monitor.css';
export const Monitor = ()=>{
    return (
        <div className="container">
            <LastPassword/>
            <hr className="solid"></hr>
            <NextList/>
        </div>
    )
}
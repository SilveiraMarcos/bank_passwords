import { useSelector } from 'react-redux';
import './LastPassword.css';
export const LastPassword = ()=>{

    const Last = useSelector((state)=> state.passwords.password);

    return (
        <>
            <h1>{Last? Last[Last.length -1].last:"0000"}</h1>
        </>
    )
}